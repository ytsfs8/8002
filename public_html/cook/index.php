<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>
            りょうりのなまえ by ゆい 【ずぼらパッド】 簡単おいしいずぼらレシピがいっぱい
        </title>
        <link rel="stylesheet" media="all" href="./css/print.css" />
        <link rel="stylesheet" media="print" href="./css/print_tool.css" />
        <link rel="canonical" href="http://cookpad.com/recipe/4906886">
    </head>
    <body id="print_page">
        <div id="print_container" class="print-medium">
            <div id="header_container">
                <div id="title">
                    <table>
                        <tr>
                            <td width="160">
                                <img src="https://assets.cpcdn.com/assets/themes/kitchen/logo_print_s.gif?31d35ea939d17b778c0ce362ead81fae368c6162b5476a36689498817f48067f" alt="Logo print s" />
                            </td>
                            <td width="400">
                                <h1>
                                    <span id="recipe_title">りょうりのなまえ</span>
                                </h1>
                                <p class="author">
                                    by ゆい
                                </p>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <hr />
            <div id="photo_container">
                <div id="main_photo">
                    <img width="180" alt="りょうりのなまえの写真" src="https://img.cpcdn.com/recipes/4906886/180/6eb3a36b05ca73ccb65391b47ebda522.jpg?u=12244435&amp;p=1517045897" />
                </div>
                <div id="descriptions">
                    <div id="description_container">
                        <table>
                            <tr>
                                <td>
                                    <div id="description">
                                        <div id="description_header">
                                            <img src="https://assets.cpcdn.com/assets/themes/recipe/description_header.gif?1653e711d293381fdedfbbf16224b7428912be6486faf82fadb72a2efb8f3170" alt="Description header" />
                                        </div>
                                        <div id="description_cont">
                                            全卵立てで仕上げたガトーショコラ。ヨーグルト使用でカロリー少しoff?
                                        </div>
                                        <div id="description_footer">
                                            <img src="https://assets.cpcdn.com/assets/themes/recipe/description_footer.gif?42c1cfc248dc65d3044ba05f38c5eb40db63b81053a7e1b4964e1486d1a09f0f" alt="Description footer" />
                                        </div>
                                    </div>
                                </td>
                                <td valign="bottom">
                                    <img src="https://img.cpcdn.com/users/12244435/48x48c/466c5f8b70a8f9f8767c35c9f1943aa7.jpg?u=12244435&amp;p=1483320632" width="48" height="48" alt="ゆいさんの写真" id="author-photo" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="description_text" id="point">
                        <h2>
                            コツ、ポイント
                        </h2>
                        <p>
                            湯煎している時に卵を立てる。焼いている時に片付ける。およそ焼き上がりまで1時間。
                        </p>
                    </div>
                </div>
            </div>
            <div id="ingredients_steps_container">
                <div id="ingredients_wrapper">
                    <div id="servings">
                        <h2>
                            材料：
                        </h2>
                        <p>
                            (15cm(5号))
                        </p>
                    </div>
                    <table id="ingredients_list" summary="材料一覧" cellpadding="0" cellspacing="0">
                        <tr>
                            <th id="ingredient_46750653_name" class="ingredient_nanme"  >
                                板チョコ
                            </th>
                            <td id="ingredient__46750653_quantity" class="ingredient_quantity">
                                2枚(100g)
                            </td>
                        </tr>
                        <tr>
                            <th id="ingredient_46750654_name" class="ingredient_nanme"  >
                                卵
                            </th>
                            <td id="ingredient__46750654_quantity" class="ingredient_quantity">
                                2個
                            </td>
                        </tr>
                        <tr>
                            <th id="ingredient_46750655_name" class="ingredient_nanme"  >
                                砂糖
                            </th>
                            <td id="ingredient__46750655_quantity" class="ingredient_quantity">
                                50g
                            </td>
                        </tr>
                        <tr>
                            <th id="ingredient_46750656_name" class="ingredient_nanme"  >
                                薄力粉
                            </th>
                            <td id="ingredient__46750656_quantity" class="ingredient_quantity">
                                50g
                            </td>
                        </tr>
                        <tr>
                            <th id="ingredient_46750657_name" class="ingredient_nanme"  >
                                プレーンヨーグルト
                            </th>
                            <td id="ingredient__46750657_quantity" class="ingredient_quantity">
                                大さじ3
                            </td>
                        </tr>
                        <tr>
                            <th id="ingredient_46750658_name" class="ingredient_nanme"  >
                                洋酒
                            </th>
                            <td id="ingredient__46750658_quantity" class="ingredient_quantity">
                                適量
                            </td>
                        </tr>
                        <tr>
                            <th id="ingredient_46750659_name" class="ingredient_nanme"  >
                                粉砂糖
                            </th>
                            <td id="ingredient__46750659_quantity" class="ingredient_quantity">
                                適量
                            </td>
                        </tr>
                    </table>
                    <div class="description_text" id="history">
                        <h2>
                            このレシピの生い立ち
                        </h2>
                        <p>
                            もうすぐ2月です。練習も兼ねてカロリーoffでも美味しいか試してみました❗
                        </p>
                    </div>
                </div>
                <div id="steps">
                    <table>
                        <tr>
                            <td valign="top" class="step_index">
                                <strong>1.</strong>
                            </td>
                            <td class="step_memo" valign="top" >
                                材料を揃えます。
                            </td>
                            <td valign="top" align="right" class="step_memo_image_container">
                                <img width="100" alt="作り方1の写真" src="https://img.cpcdn.com/steps/24040172/100/878b2e622c06f509af0d718176082e11.jpg?u=12244435&amp;p=1517046544" />
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" class="step_index">
                                <strong>2.</strong>
                            </td>
                            <td class="step_memo" valign="top" >
                                板チョコとプレーンヨーグルトを湯煎にして混ぜ合わせます。(電子レンジでも可)
                            </td>
                            <td valign="top" align="right" class="step_memo_image_container">
                                <img width="100" alt="作り方2の写真" src="https://img.cpcdn.com/steps/24041063/100/45958a09e828477605b2a46a7dc244b9.jpg?u=12244435&amp;p=1517054299" />
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" class="step_index">
                                <strong>3.</strong>
                            </td>
                            <td class="step_memo" valign="top" >
                                薄力粉をふるます。
                            </td>
                            <td valign="top" align="right" class="step_memo_image_container">
                                <img width="100" alt="作り方3の写真" src="https://img.cpcdn.com/steps/24041064/100/23bba0097a494a4b35c5577de2cd583b.jpg?u=12244435&amp;p=1517054301" />
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" class="step_index">
                                <strong>4.</strong>
                            </td>
                            <td class="step_memo" valign="top" >
                                型にクッキングシートを敷く
                            </td>
                            <td valign="top" align="right" class="step_memo_image_container">
                                <img width="100" alt="作り方4の写真" src="https://img.cpcdn.com/steps/24041065/100/6796fa0f7ba2599e86912b9b455ebe07.jpg?u=12244435&amp;p=1517054303" />
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" class="step_index">
                                <strong>5.</strong>
                            </td>
                            <td class="step_memo" valign="top" >
                                卵2個を全卵立てにして3回に分けて砂糖を入れる。
                            </td>
                            <td valign="top" align="right" class="step_memo_image_container">
                                <img width="100" alt="作り方5の写真" src="https://img.cpcdn.com/steps/24041067/100/5b3dae73ea905d2740c8f50e832eb048.jpg?u=12244435&amp;p=1517054305" />
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" class="step_index">
                                <strong>6.</strong>
                            </td>
                            <td class="step_memo" valign="top" >
                                卵の角が立ったら、薄力粉と湯煎し混ぜ合わせたチョコレートをさくっと合わせる
                            </td>
                            <td valign="top" align="right" class="step_memo_image_container">
                                <img width="100" alt="作り方6の写真" src="https://img.cpcdn.com/steps/24042839/100/90f21a7cdf76ffd77d5bb89e29d211ed.jpg?u=12244435&amp;p=1517089905" />
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" class="step_index">
                                <strong>7.</strong>
                            </td>
                            <td class="step_memo" valign="top" >
                                均一になったら型に流して軽く揺すって大きな気泡をとる。
                                お好きな方は香付けに洋酒を適量入れて下さい。
                            </td>
                            <td valign="top" align="right" class="step_memo_image_container">
                                <img width="100" alt="作り方7の写真" src="https://img.cpcdn.com/steps/24042846/100/41e28aa326a4de8a098db9da3983d828.jpg?u=12244435&amp;p=1517090084" />
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" class="step_index">
                                <strong>8.</strong>
                            </td>
                            <td class="step_memo" valign="top" >
                                あらかじめ170度にしておいたオーブンで35分で焼きかげんを竹串等で確認。
                                型からはずしす。
                            </td>
                            <td valign="top" align="right" class="step_memo_image_container">
                                <img width="100" alt="作り方8の写真" src="https://img.cpcdn.com/steps/24042859/100/f1ac62a56802d4b93e6777341cb9c59a.jpg?u=12244435&amp;p=1517090332" />
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" class="step_index">
                                <strong>9.</strong>
                            </td>
                            <td class="step_memo" valign="top" >
                                一日おいて粉砂糖で化粧を施し完成!
                            </td>
                            <td valign="top" align="right" class="step_memo_image_container">
                                <img width="100" alt="作り方9の写真" src="https://img.cpcdn.com/steps/24042864/100/57451e63a6a31eec969eac1aacc31542.jpg?u=12244435&amp;p=1517090436" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" id="kitchen_url">
                                https://cookpad.com/recipe/4906886
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <hr/>
            <table width="100%" cellspacing="0" cellpadding="0" id="footer_table">
                <tr>
                    <td align="center">
                        Copyright© Cookpad Inc. All Rights Reserved.
                    </td>
                </tr>
            </table>
        </div>
    </body>
</html>
