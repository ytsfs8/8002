<?php
    $kana = array(
        "1" => "あ",
        "2" => "い",
        "3" => "う",
        "4" => "え",
        "5" => "お",
        "6" => "か",
        "7" => "き",
        "8" => "く",
        "9" => "け",
        "10" => "こ",
        "11" => "さ",
        "12" => "し",
        "13" => "す",
        "14" => "せ",
        "15" => "そ",
        "16" => "た",
        "17" => "ち",
        "18" => "つ",
        "19" => "て",
        "20" => "と",
        "21" => "な",
        "22" => "に",
        "23" => "ぬ",
        "24" => "ね",
        "25" => "の",
        "26" => "は",
        "27" => "ひ",
        "28" => "ふ",
        "29" => "へ",
        "30" => "ほ",
        "31" => "ま",
        "32" => "み",
        "33" => "む",
        "34" => "め",
        "35" => "も",
        "36" => "や",
        "37" => "ゆ",
        "38" => "よ",
        "39" => "ら",
        "40" => "り",
        "41" => "る",
        "42" => "れ",
        "43" => "ろ",
        "44" => "わ",
        "45" => "を",
        "46" => "が",
        "47" => "ぎ",
        "48" => "ぐ",
        "49" => "げ",
        "50" => "ご",
        "51" => "ざ",
        "52" => "じ",
        "53" => "ず",
        "54" => "ぜ",
        "55" => "ぞ",
        "56" => "だ",
        "57" => "ぢ",
        "58" => "づ",
        "59" => "で",
        "60" => "ど",
        "61" => "ば",
        "62" => "び",
        "63" => "ぶ",
        "64" => "べ",
        "65" => "ぼ",
        "66" => "ぱ",
        "67" => "ぴ",
        "68" => "ぷ",
        "69" => "ぺ",
        "70" => "ぽ",
        "71" => "ん",
    );
    

    $morusu = array(
        "1" => "－－・－－",
        "2" => "・－",
        "3" => "・・－",
        "4" => "－・－－－",
        "5" => "・－・・・",
        "6" => "・－・・",
        "7" => "－・－・・",
        "8" => "・・・－",
        "9" => "－・－－",
        "10" => "－－－－",
        "11" => "－・－・－",
        "12" => "－－・－・",
        "13" => "－－－・－",
        "14" => "・－－－・",
        "15" => "－－－・",
        "16" => "－・",
        "17" => "・・－・",
        "18" => "・－－・",
        "19" => "・－・－－",
        "20" => "・・－・・",
        "21" => "・－・",
        "22" => "－・－・",
        "23" => "・・・・",
        "24" => "－－・－",
        "25" => "・・－－",
        "26" => "－・・・",
        "27" => "－－・・－",
        "28" => "－－・・",
        "29" => "・",
        "30" => "－・・",
        "31" => "－・・－",
        "32" => "・・－・－",
        "33" => "－",
        "34" => "－・・・－",
        "35" => "－・・－・",
        "36" => "・－－",
        "37" => "－・・－－",
        "38" => "－－",
        "39" => "・・・",
        "40" => "－－・",
        "41" => "－・－－・",
        "42" => "－－－",
        "43" => "・－・－",
        "44" => "－・－",
        "45" => "・－－－",
        "46" => "・－・・ ・・",
        "47" => "－・－・・ ・・",
        "48" => "・・・－ ・・",
        "49" => "－・－－ ・・",
        "50" => "－－－－ ・・",
        "51" => "－・－・－ ・・",
        "52" => "－－・－・ ・・",
        "53" => "－－－・－ ・・",
        "54" => "・－－－・ ・・",
        "55" => "－－－・ ・・",
        "56" => "－・ ・・",
        "57" => "・・－・ ・・",
        "58" => "・－－・ ・・",
        "59" => "・－・－－ ・・",
        "60" => "・・－・・ ・・",
        "61" => "－・・・ ・・",
        "62" => "－－・・－ ・・",
        "63" => "－－・・ ・・",
        "64" => "・ ・・",
        "65" => "－・・ ・・",
        "66" => "－・・・ ・・－－・",
        "67" => "－－・・－ ・・－－・",
        "68" => "－－・・ ・・－－・",
        "69" => "・ ・・－－・",
        "70" => "－・・ ・・－－・",
        "71" => "・－・－・",
    );

    $str_kana   = $_POST['kana'];
    $str_morusu = $_POST['morusu'];

    $array_kana = array();
    $array_kana = preg_split("//u", $str_kana, -1, PREG_SPLIT_NO_EMPTY);
    // PREG_SPLIT_NO_EMPTY ：配列の最初と最後に空データが含まれてしまうのを防ぐ

    foreach ($array_kana as $key_kana => $value_kana) {
        $newphrase_kana[$key_kana] = array_search($value_kana, $morusu_kana);
    }
    $view_morusu = implode(" ", $newphrase_kana);


    if (empty($str_morusu) === false) {
        $matches_morusu = explode(" ", $str_morusu);
    }

    $matches_morusu = array_filter($matches_morusu, "strlen");
    $matches_morusu = array_values($matches_morusu);
    foreach ($matches_morusu as $key => $value) {
        if ($value === "・・") {
            $tmp_daku                 = array();
            $tmp_daku                 = array( $matches_morusu[$key - 1],
                                               $matches_morusu[$key] );
            $tmp_daku                 = implode(" ", $tmp_daku);
            $matches_morusu[$key - 1] = $tmp_daku;
            $matches_morusu[$key]     = "";
        } else {
            if ($value === "・・－－・") {
                $tmp_handaku              = array();
                $tmp_handaku              = array( $matches_morusu[$key - 1],
                                                   $matches_morusu[$key] );
                $tmp_handaku              = implode(" ", $tmp_handaku);
                $matches_morusu[$key - 1] = $tmp_handaku;
                $matches_morusu[$key]     = "";
            }
        }
    }
    $matches_morusu = array_filter($matches_morusu, "strlen");
    $matches_morusu = array_values($matches_morusu);

    foreach ($matches_morusu as $key_morusu => $value_morusu) {
        $newphrase_morusu[$key_morusu] = array_search($value_morusu, $kana_morusu);
    }
    $view_kana = implode("", $newphrase_morusu);
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>モールス変換機</title>
        <link rel="stylesheet" type="text/css" href="../umi/css/bootstrap.css">
        <style>
            <!--
            body{margin:50px;width:1000px;}
            div.form{float:left; padding:50px; width:100%;}
            textarea{margin:10px 0;}
            span{font-size: 20pt;}
            -->
        </style>
    </head>
    <body>
        <form action="" method="post">
            <div class="form">
                <span>ひらがな→モールス信号<br /></span>
                <input type="text" name="kana" size="40" value="<?php echo($_POST['kana']); ?>"/>
                <input type="submit" value="変換">
                <br/>
                <textarea rows="4" cols="40" readonly/><?php print($view_morusu); ?></textarea>
            </div>

            <div class="form">
                <span>モールス信号→ひらがな<br /></span>
                <input type="text" name="morusu" size="40" value="<?php echo($_POST['morusu']); ?>"/>
                <input type="submit" value="復元">
                <br/>
                <textarea rows="4" cols="40" readonly/><?php print($view_kana); ?></textarea>
            </div>
        </form>
    </body>
</html>